'use strict';

angular.module('app.logging', [])

    .service('airLogging', [function(){
        return function(action) {
            console.log(action);
        }
    }]);
