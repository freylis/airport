from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',

    url(r'^admin/', include(admin.site.urls)),

    # my includes
    url(r'^port/', include('airplane.apps.port.urls')),
    url(r'^plane/', include('airplane.apps.plane.urls')),
    url(r'^route/', include('airplane.apps.route.urls')),

    url(r'^$', 'airplane.views.home'),
)
