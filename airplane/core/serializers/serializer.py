# coding: utf-8
from django.core import serializers

from rest_framework.renderers import JSONRenderer


serializer_object = serializers.get_serializer('json')


def serialize_queryset(qs):

    json_serializer = serializer_object()
    json_serializer.serialize(qs)
    data = json_serializer.getvalue()
    json_data = JSONRenderer().render(data)
    a = json_data
    print dir(a), type(a)
    return []