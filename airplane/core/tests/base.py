# coding: utf-8

from django.core.urlresolvers import reverse


class UrlMaker(object):

    def __init__(self):
        pass

    @staticmethod
    def make_url(viewname, **kwargs):
        return reverse(viewname=viewname, kwargs=kwargs)