# coding: utf-8
import pytest

from airplane.core.tests.base import UrlMaker
from airplane.apps.plane.tests.conftest import create_manufacturer
from airplane.apps.plane.tests.conftest import create_plane_model
from airplane.apps.plane.tests.conftest import create_plane_modification
from airplane.apps.plane.tests.conftest import create_plane
from airplane.apps.plane.tests.conftest import PlaneConstructor
from airplane.apps.port.tests.conftest import create_port
from airplane.apps.port.tests.conftest import PortConstructor
from airplane.apps.route.tests.conftest import create_route
from airplane.apps.route.tests.conftest import RouteConstructor


@pytest.fixture
def url_maker():
    return UrlMaker


####################
# creating fixtures
####################


# about plane
manufacturer = pytest.fixture(create_manufacturer)
plane_model = pytest.fixture(create_plane_model)
plane_modification = pytest.fixture(create_plane_modification)
plane = pytest.fixture(create_plane)

@pytest.fixture
def plane_constructor():
    return PlaneConstructor


# about port
port = pytest.fixture(create_port)

@pytest.fixture
def port_constructor():
    return PortConstructor


# about route

route = pytest.fixture(create_route)

@pytest.fixture
def route_constructor():
    return RouteConstructor