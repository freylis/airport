# coding: utf-8

from rest_framework import status
from airplane.core.exceptions.exceptions import AirplaneException


class PortDoesNotExistException(AirplaneException):
    message = 'port does not exist'
    status_code = status.HTTP_404_NOT_FOUND