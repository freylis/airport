from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^api/ports$', 'airplane.apps.port.api.ports_list', name='api_get_ports_list'),
    url(r'^api/ports/(?P<port_id>\d+)', 'airplane.apps.port.api.port_by_id', name='api_get_port_by_id'),
)
