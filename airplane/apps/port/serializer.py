# coding: utf-8

from rest_framework import serializers

from .models import Port


class PortSerializer(serializers.ModelSerializer):

    class Meta:
        model = Port


def serialize_port_queryset(queryset):

    serializer = PortSerializer(queryset, many=True)
    return serializer.data


def serialize_port(port):

    serializer = PortSerializer(port)
    return serializer.data