# coding: utf-8

import json

import pytest


class TestPortClient(object):

    @pytest.mark.django_db
    def test_client_port_by_id(self, client, url_maker, port):

        url = url_maker.make_url('api_get_port_by_id', port_id=port.id)
        response = client.get(url)
        assert response.status_code == 200

        response_data = json.loads(response.content)

        assert response_data['success']
        assert isinstance(response_data['result']['port'], dict)
        assert response_data['result']['port']

    @pytest.mark.django_db
    def test_client_ports_list(self, client, url_maker, port_constructor):

        port_constructor.create_port()
        port_constructor.create_port()

        url = url_maker.make_url('api_get_ports_list')
        response = client.get(url)
        response_data = json.loads(response.content)

        assert response_data['success']
        assert isinstance(response_data['result']['ports'], list)
        assert len(response_data['result']['ports']) == 2