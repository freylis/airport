# coding: utf-8

from ..models import Port


def create_port():
    return Port.objects.create(name='Koltsovo', latitude=100, longitude=150)


class PortConstructor(object):

    @staticmethod
    def create_port():
        return create_port()