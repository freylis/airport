# coding: utf-8
import json
import pytest

from ..models import Port
from ..serializer import serialize_port_queryset
from ..serializer import serialize_port


class TestPort(object):

    @pytest.mark.django_db
    def test_serialize_port(self, port):
        port_data = serialize_port(port)
        assert isinstance(port_data, dict)

    @pytest.mark.django_db
    def test_serialize_queryset(self, port_constructor):

        port_constructor.create_port()
        port_constructor.create_port()
        port_constructor.create_port()

        ports_qs = Port.objects.all()
        serialized_data = serialize_port_queryset(ports_qs)

        assert isinstance(serialized_data, list)
        assert len(serialized_data) == 3