# coding: utf-8

from django.utils.translation import ugettext_lazy as _

from django.db import models

from airplane.core.models import BaseModel


class Port(BaseModel):

    class Meta:
        verbose_name = _(u'Аэропорт')
        verbose_name_plural = _(u'Аэропорты')

    name = models.CharField(_(u'Страна'), null=True, blank=False, max_length=100)
    latitude = models.PositiveIntegerField(_(u'Широта'), null=True, blank=False)
    longitude = models.PositiveIntegerField(_(u'Долгота'), null=True, blank=False)

    def __unicode__(self):
        return self.name