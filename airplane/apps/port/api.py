# coding: utf-8

from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Port
from .utils import get_ports_list
from .utils import get_port_info
from .exceptions import PortDoesNotExistException


@api_view(['GET'])
def ports_list(request):
    ports = get_ports_list()
    return Response({
        'success': True,
        'result': {
            'ports': ports,
        },
    })


@api_view(['GET'])
def port_by_id(request, port_id):
    try:
        port = Port.objects.get(id=port_id)
    except Port.DoesNotExist:
        raise PortDoesNotExistException()

    port_info = get_port_info(port)

    return Response({
        'success': True,
        'result': {
            'port': port_info,
        }
    })