# coding: utf-8

from django.contrib import admin

from .models import Port


class PortAdmin(admin.ModelAdmin):
    pass
admin.site.register(Port, PortAdmin)