# coding: utf-8

from .serializer import serialize_port_queryset
from .serializer import serialize_port
from .models import Port


def get_ports_list(**conditions):

    ports_qs = Port.all().filter(**conditions)
    return serialize_port_queryset(ports_qs)


def get_port_info(port):
    return serialize_port(port)