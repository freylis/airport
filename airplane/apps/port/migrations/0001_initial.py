# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Port',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, verbose_name='\u0421\u0442\u0440\u0430\u043d\u0430')),
                ('latitude', models.PositiveIntegerField(null=True, verbose_name='\u0428\u0438\u0440\u043e\u0442\u0430')),
                ('longitude', models.PositiveIntegerField(null=True, verbose_name='\u0414\u043e\u043b\u0433\u043e\u0442\u0430')),
            ],
            options={
                'verbose_name': '\u0410\u044d\u0440\u043e\u043f\u043e\u0440\u0442',
                'verbose_name_plural': '\u0410\u044d\u0440\u043e\u043f\u043e\u0440\u0442\u044b',
            },
            bases=(models.Model,),
        ),
    ]
