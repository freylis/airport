# coding: utf-8

import json

import pytest


class TestPlaneClient(object):

    @pytest.mark.django_db
    def test_client_planes_list(self, client, url_maker, plane_constructor):

        # create two planes
        plane_constructor.create_plane()
        plane_constructor.create_plane()

        url = url_maker.make_url('api_get_planes_list')
        response = client.get(url)
        assert response.status_code == 200

        response_data = json.loads(response.content)
        assert response_data['success']
        assert isinstance(response_data['result']['planes'], list)
        assert len(response_data['result']['planes']) == 2

    @pytest.mark.django_db
    def test_client_plane_info(self, client, url_maker, plane):

        url = url_maker.make_url('api_get_plane_info', plane_id=plane.id)
        response = client.get(url)
        assert response.status_code == 200

        response_data = json.loads(response.content)
        assert response_data['success']
        assert isinstance(response_data['result']['plane'], dict)
        assert response_data['result']['plane']
