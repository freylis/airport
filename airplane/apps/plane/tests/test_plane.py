# coding: utf-8
import json
import pytest

from ..utils import get_plane_info
from ..serializer import serialize_plane_queryset
from ..models import Plane


class TestPlane(object):

    @pytest.mark.django_db
    def test_planes_serialize(self, plane_constructor):

        # create two planes
        plane_constructor.create_plane()
        plane_constructor.create_plane()

        serialized_planes = serialize_plane_queryset(Plane.all())
        assert len(serialized_planes) == 2

    @pytest.mark.django_db
    def test_should_check_plane_info(self, client, url_maker, plane):

        # get user info
        plane_info = get_plane_info(plane)
        assert plane_info

        url = url_maker.make_url('api_get_plane_info', plane_id=plane.id)
        response = client.get(url)

        assert response.status_code == 200
        response_data = json.loads(response.content)

        assert response_data['success']
        assert isinstance(response_data['result']['plane'], dict)
