# coding: utf-8

from ..models import Plane
from ..models import Modification
from ..models import Model
from ..models import Manufacturer


def create_manufacturer():

    return Manufacturer.objects.create(name='Boeing')


def create_plane_model():

    manufacturer = create_manufacturer()
    return Model.objects.create(name='777', manufacturer=manufacturer)


def create_plane_modification():

    plane_model = create_plane_model()
    return Modification.objects.create(name='777-800', model=plane_model)


def create_plane():

    plane_modification = create_plane_modification()
    return Plane.objects.create(name='Mishko`s plane', modification=plane_modification)


class PlaneConstructor(object):

    @staticmethod
    def create_manufacturer():
        return create_manufacturer()

    @staticmethod
    def create_plane_model():
        return create_plane_model()

    @staticmethod
    def create_plane_modification():
        return create_plane_modification()

    @staticmethod
    def create_plane():
        return create_plane()
