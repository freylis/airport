# coding: utf-8

from django.contrib import admin

from .models import Manufacturer
from .models import Model
from .models import Modification
from .models import Plane


class ManufacturerAdmin(admin.ModelAdmin):
    pass
admin.site.register(Manufacturer, ManufacturerAdmin)


class PlaneModelAdmin(admin.ModelAdmin):
    pass
admin.site.register(Model, PlaneModelAdmin)


class PlaneModelModificationAdmin(admin.ModelAdmin):
    pass
admin.site.register(Modification, PlaneModelModificationAdmin)


class PlaneAdmin(admin.ModelAdmin):
    pass
admin.site.register(Plane, PlaneAdmin)