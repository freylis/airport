# coding: utf-8

from rest_framework import serializers

from .models import Plane
from .models import Model
from .models import Manufacturer
from .models import Modification


class ManufacturerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Manufacturer


def serialize_manufacturer_queryset(queryset):

    serializer = ManufacturerSerializer(queryset, many=True)
    return serializer.data


def serialize_manufacturer(manufacturer):

    serializer = ManufacturerSerializer(manufacturer)
    return serializer.data


class ModelSerializer(serializers.ModelSerializer):

    manufacturer = ManufacturerSerializer()

    class Meta:
        model = Model


def serialize_plane_model_queryset(queryset):

    serializer = ModelSerializer(queryset, many=True)
    return serializer.data


def serialize_plane_model(plane_model):

    serializer = ModelSerializer(plane_model)
    return serializer.data


class ModificationSerializer(serializers.ModelSerializer):

    model = ModelSerializer()

    class Meta:
        model = Modification


def serialize_plane_modification_queryset(queryset):

    serializer = ModificationSerializer(queryset, many=True)
    return serializer.data


def serialize_plane_modification(plane_model_modification):

    serializer = ModificationSerializer(plane_model_modification)
    return serializer.data


class PlaneSerializer(serializers.ModelSerializer):

    modification = ModificationSerializer()

    class Meta:
        model = Plane


def serialize_plane_queryset(queryset):

    serializer = PlaneSerializer(queryset, many=True)
    return serializer.data


def serialize_plane(plane):

    serializer = PlaneSerializer(plane)
    return serializer.data
