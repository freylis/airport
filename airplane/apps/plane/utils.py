# coding: utf-8

from .models import Plane
from .serializer import serialize_plane_queryset
from .serializer import serialize_plane


def get_planes_list(**conditions):

    planes = Plane.all().filter(**conditions)
    return serialize_plane_queryset(queryset=planes)


def get_plane_info(plane):

    return serialize_plane(plane)