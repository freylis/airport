from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^api/planes$', 'airplane.apps.plane.api.planes_list', name='api_get_planes_list'),
    url(r'^api/planes/(?P<plane_id>\d+)$', 'airplane.apps.plane.api.plane_info', name='api_get_plane_info'),
)
