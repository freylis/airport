# coding: utf-8

from django.utils.translation import ugettext_lazy as _

from rest_framework import status as http_statuses

from airplane.core.exceptions.exceptions import AirplaneException


class PlaneDoesNotExistException(AirplaneException):
    message = _(u'Самолет не найден')
    status_code = http_statuses.HTTP_404_NOT_FOUND
