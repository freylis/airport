# coding: utf-8

from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import Plane
from .utils import get_plane_info
from .utils import get_planes_list
from .exceptions.exceptions import PlaneDoesNotExistException


@api_view(['GET'])
def planes_list(request):
    """
    get all planes
    """
    planes_list = get_planes_list()
    return Response({
        'success': True,
        'result': {
            'planes': planes_list,
        }
    })


@api_view(['GET'])
def plane_info(request, plane_id):
    """
    information about plane
    """
    try:
        plane = Plane.all().get(id=plane_id)
    except Plane.DoesNotExist:
        raise PlaneDoesNotExistException()

    plane_info = get_plane_info(plane)

    return Response({
        'success': True,
        'result': {
            'plane': plane_info,
        },
    })