# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from django.db import models

from airplane.core.models import BaseModel


class Manufacturer(BaseModel):

    class Meta:
        verbose_name = _(u'Производитель')
        verbose_name_plural = _(u'Производители')

    name = models.CharField(_(u'Название компании'), null=True, blank=False, max_length=100)

    def __unicode__(self):
        return self.name

    def get_title(self):
        return self.name


class Model(BaseModel):

    class Meta:
        verbose_name = _(u'Модель самолета')
        verbose_name_plural = _(u'Модели самолетов')

    name = models.CharField(_(u'Название модели'), null=True, blank=False, max_length=100)
    manufacturer = models.ForeignKey('plane.Manufacturer', null=True, blank=False, verbose_name=_(u'Производитель'))

    def __unicode__(self):
        return self.name

    def get_title(self):
        return self.name


class Modification(BaseModel):

    class Meta:
        verbose_name = _(u'Модификация самолета')
        verbose_name_plural = _(u'Модификации самолетов')

    name = models.CharField(_(u'Название модификации'), null=True, blank=False, max_length=100)
    model = models.ForeignKey('plane.Model', null=True, blank=False, verbose_name=_(u'Модель самолета'))

    def __unicode__(self):
        return self.name

    def get_title(self):
        return self.name


class Plane(BaseModel):

    class Meta:
        verbose_name = _(u'Самолет')
        verbose_name_plural = _(u'Самолеты')

    name = models.CharField(_(u'Название самолета'), null=True, blank=False, max_length=100)
    modification = models.ForeignKey('plane.Modification', null=True, blank=False, verbose_name=_(u'Модель самолета'))

    def __unicode__(self):
        return self.name

    def get_title(self):
        return self.name
