# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('plane', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='plane',
            old_name='model',
            new_name='modification',
        ),
    ]
