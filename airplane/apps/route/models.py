# coding: utf-8

from django.utils.translation import ugettext_lazy as _
from django.db import models

from airplane.core.models import BaseModel


class Route(BaseModel):

    class Meta:
        verbose_name = _(u'Маршрут')
        verbose_name_plural = _(u'Маршруты')

    code = models.CharField(_(u'Код маршрута'), null=True, blank=False, max_length=100)

    def __unicode__(self):
        return self.code

    def get_title(self):
        return self.code


class Path(BaseModel):

    class Meta:
        verbose_name = _(u'Участок маршрута')
        verbose_name_plural = _(u'Участки маршрута')

    route = models.ForeignKey('route.Route', null=True, blank=False, verbose_name=_(u'Маршрут'),
                              related_name='paths')
    port = models.ForeignKey('port.Port', null=True, blank=False, verbose_name=_(u'Аэропорт'),
                             related_name='ports')

    def __unicode__(self):
        try:
            return '%s (%s)' % (self.route.get_title(), self.port.get_title())
        except AttributeError:
            return '%s' % self.id