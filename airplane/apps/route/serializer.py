# coding: utf-8

from rest_framework import serializers

from airplane.apps.port.serializer import PortSerializer
from .models import Route
from .models import Path


class RoutePathSerializer(serializers.ModelSerializer):

    port = PortSerializer()

    class Meta:
        model = Path


def serialize_route_path_queryset(queryset):

    serializer = RoutePathSerializer(queryset, many=True)
    return serializer.data


def serialize_route_path(route_path):

    serializer = RoutePathSerializer(route_path)
    return serializer.data


class RouteSerializer(serializers.ModelSerializer):

    paths = RoutePathSerializer(many=True)

    class Meta:
        model = Route


def serialize_route_queryset(queryset):

    serializer = RouteSerializer(queryset, many=True)
    return serializer.data


def serialize_route(route):

    serializer = RouteSerializer(route)
    return serializer.data