# coding: utf-8

from airplane.apps.port.tests.conftest import create_port
from ..models import Route
from ..models import Path


def create_route(code='01'):
    route = Route.objects.create(code=code)

    # create start & end route paths
    port_start = create_port()
    port_end = create_port()
    Path.objects.create(route=route, port=port_start)
    Path.objects.create(route=route, port=port_end)
    return route


class RouteConstructor(object):

    @staticmethod
    def create_route():
        return create_route()