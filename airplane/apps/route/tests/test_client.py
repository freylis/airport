# coding: utf-8

import json
import pytest


class TestRouteClient(object):

    @pytest.mark.django_db
    def test_client_routs_list(self, client, url_maker, route_constructor):

        route_constructor.create_route()
        route_constructor.create_route()

        url = url_maker.make_url('api_get_routes')
        response = client.get(url)

        assert response.status_code == 200
        response_data = json.loads(response.content)
        assert response_data['success']
        assert isinstance(response_data['result']['routes'], list)
        assert len(response_data['result']['routes']) == 2


    @pytest.mark.django_db
    def test_client_route_info(self, client, url_maker, route):

        url = url_maker.make_url('api_get_route_info', route_id=route.id)
        response = client.get(url)

        assert response.status_code == 200
        response_data = json.loads(response.content)

        assert response_data['success']
        assert isinstance(response_data['result']['route'], dict)
        assert response_data['result']['route']
