# coding: utf-8

import pytest

from ..utils import get_routes_list
from ..utils import get_route_info


class TestRoute(object):

    @pytest.mark.django_db
    def test_get_route_info(self, route):

        assert isinstance(get_route_info(route), dict)

    @pytest.mark.django_db
    def test_get_routes_list(self, route_constructor):

        route_constructor.create_route()
        route_constructor.create_route()
        route_constructor.create_route()

        routes_info_list = get_routes_list()
        assert isinstance(routes_info_list, list)
        assert len(routes_info_list) == 3