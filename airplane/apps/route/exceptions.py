# coding: utf-8
from rest_framework import status as http_status

from django.utils.translation import ugettext_lazy as _

from airplane.core.exceptions.exceptions import AirplaneException


class RouteDoesNotExist(AirplaneException):
    message = _(u'Маршрут не существует')
    status_code = http_status.HTTP_404_NOT_FOUND