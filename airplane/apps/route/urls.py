# coding: utf-8

from django.conf.urls import patterns, url


urlpatterns = patterns('',
    url(r'^api/routes$', 'airplane.apps.route.api.routes_list', name='api_get_routes'),
    url(r'^api/routes/(?P<route_id>\d+)$', 'airplane.apps.route.api.route_info', name='api_get_route_info'),
)
