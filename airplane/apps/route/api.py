# coding: utf-8

from rest_framework.decorators import api_view
from rest_framework.response import Response

from .exceptions import RouteDoesNotExist
from .models import Route
from .utils import get_route_info
from .utils import get_routes_list


@api_view(['GET'])
def routes_list(request):

    routes = get_routes_list()

    return Response({
        'success': True,
        'result': {
            'routes': routes,
        }
    })


@api_view(['GET'])
def route_info(request, route_id):

    try:
        route = Route.objects.get(id=route_id)
    except Route.DoesNotExist:
        raise RouteDoesNotExist()

    info = get_route_info(route)
    return Response({
        'success': True,
        'result': {
            'route': info,
        }
    })

