# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('port', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Path',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('port', models.ForeignKey(related_name='ports', verbose_name='\u0410\u044d\u0440\u043e\u043f\u043e\u0440\u0442', to='port.Port', null=True)),
            ],
            options={
                'verbose_name': '\u0423\u0447\u0430\u0441\u0442\u043e\u043a \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430',
                'verbose_name_plural': '\u0423\u0447\u0430\u0441\u0442\u043a\u0438 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=100, null=True, verbose_name='\u041a\u043e\u0434 \u043c\u0430\u0440\u0448\u0440\u0443\u0442\u0430')),
            ],
            options={
                'verbose_name': '\u041c\u0430\u0440\u0448\u0440\u0443\u0442',
                'verbose_name_plural': '\u041c\u0430\u0440\u0448\u0440\u0443\u0442\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='path',
            name='route',
            field=models.ForeignKey(related_name='paths', verbose_name='\u041c\u0430\u0440\u0448\u0440\u0443\u0442', to='route.Route', null=True),
            preserve_default=True,
        ),
    ]
