# coding: utf-8

from django.contrib import admin

from .models import Route
from .models import Path


class RoutePathInline(admin.TabularInline):
    model = Path
    extra = 2


class RouteAdmin(admin.ModelAdmin):
    inlines = [RoutePathInline]
admin.site.register(Route, RouteAdmin)