# coding: utf-8

from .serializer import serialize_route
from .serializer import serialize_route_queryset
from .models import Route


def get_routes_list(**conditions):

    routes = Route.objects.filter(**conditions)
    return serialize_route_queryset(routes)


def get_route_info(route):

    return serialize_route(route)